## Team Member Offboarding

### Introduction
An overview of the [Gitlab Offboarding Process](https://about.gitlab.com/handbook/people-group/offboarding/) can be found within the handbook. Though the People Experience Team are the Directly Responsible Individuals (DRIs) for team member offboarding there are various stakeholders that play a role in ensuring that the relevant access deprovisioning and system updates take place.

### Time Sensitivities
It is important to note that offboarding issues have a completion deadline of **five days** from date of opening i.e. five days from when the team member completes their tenure with GitLab.  

This deadline excludes access deprovisioning for core systems and tools such as GSuite, Okta, 1Password and Slack which should take place immediately in instances of involuntary termination and at the predetermined time in instances of voluntary termination.

Laptop recovery which falls within the scope of IT Operations may take longer than five days and with this in mind is exempt from the aforementioned deadline in instances where a departing team member opts to return their equipment.

### Compliance
Completion of the tasks detailed in the offboarding issue ensures that GitLab remains safeguarded and compliant.  These issues are regularly audited and reviewed making it of the utmost importance that stakeholders indicate when a task has been completed or alternatively when it does not pertain to the departing team member.

### Team Member Particulars

| | | 
| --- | --- |
| **Entity** | `__ENTITY__` |
| **Department** | `__DEPARTMENT__` | 
| **Manager** | `__MANAGER_HANDLE__` |
| **GitLab Email** | `__GITLAB_EMAIL__` | 
| **GitLab.com Handle** | `__GITLAB_HANDLE__` |
| **BAMBOOHR_ID** | `__BAMBOO_ID__` |

---

## Offboarding Tasks

<details>
<summary>Manager</summary>

1. [ ] Please comment, tagging the IT Ops team `@gitlab-com/business-ops/team-member-enablement` if you need delegate access to the former Gitlab Team Member's email account. 
1. [ ] Please comment, tagging the IT Ops team `@gitlab-com/business-ops/team-member-enablement` if you need the former Gitlab Team member's Google Drive transferred at the time of offboarding. 
1. [ ] Manager: Review all open merge requests and issues assigned to the team member and reassign them to you or another team member as per your handover agreement within the team.
1. [ ] If applicable, remove team member from any CODEOWNERS files.
1. [ ] Coordinate with IT Ops @gitlab-com/business-ops/team-member-enablement to change any shared passwords, with the intent to be gated behind Okta when possible, in particular;
   1. [ ] Review if team member had sysadmin access passwords for GitLab.com Infrastructure (ssh, chef user/key, others). Identify if any can be moved to Okta.
   1. [ ] Review what 1Password vaults former team member had access to, and identify any shared passwords to be changed and moved to Okta. 
1. [ ] In the #team-member-updates on Slack, announce departure of team member as soon as the chat and Google Accounts are blocked with the ['X is no longer with GitLab template'](https://about.gitlab.com/handbook/people-group/offboarding/#communicating-departures-company-wide). Do not say anything about reason for offboarding for involuntary offboarding.
   1. [ ] Screenshot the slack announcement and copy it in the comments of this issue. 
1. [ ] For VP and above offboarding, announce in #e-group-confidential chat channel, prior to the #team-member-update announcement, including brief context for the offboarding.
1. [ ] Remove team member from team meeting invitations.
1. [ ] Cancel weekly 1:1 meetings with team member.
1. [ ] **Important**: If the team member previously made an announcement about leaving in the #team-member-updates-channel or elsewhere, copy the link to the message/mention to your announcement in the #team-member-updates channel. If you struggle to find the message, use the search functionality in the #team-member-updates channel to find it. It is important to send this message as soon as possible through the official communication channel to ensure that the news of a departure is not disseminated in a way that works against upholding the GitLab value of transparency i.e. it is encouraged not to delay the announcement.  In instances of both voluntary and involuntary departures, it is company policy not to comment on the circumstances involved.  Team Members should be reminded to voice concerns or queries with the relevant People Business Partner or Manager however every effort must be made to uphold the confidentiality of all involved.
1. [ ] Organize a smooth hand over of any work or tasks from the former team member. Offer option to send a message to everyone in the company (i.e. forwarded by the manager), for a farewell message and/or to transmit their personal email address in case people wish to stay in touch.
1. [ ] Please reach out to your [Finance Business Partner ](https://about.gitlab.com/handbook/finance/#finance-business-partner-alignment) to discuss whether you are backfilling this role or not.
1. [ ] Scroll to the bottom of this issue; depending on the department, division, or entity there may be additional manager tasks. 

</details>

## People

<details>
<summary>People Experience</summary>

## Complete the below task ASAP 

1. [ ] People Experience: Once the offboarding has taken place (whether voluntary or involuntary), as soon as possible, create a **confidential** issue called 'Offboarding (NAME), per (DATE, please follow yyyy-mm-dd)' in in the [People Ops Employment Issue Tracker](https://gitlab.com/gitlab-com/team-member-epics/employment/issues) with relevant lines of the master offboarding checklist.
1. [ ] People Experience: For this offboarding, the manager is `__MANAGER_HANDLE__`, and People Experience is handled by `__PEOPLE_EXPERIENCE_HANDLE__`. Assign this issue to them (this should be done automatically, but worth checking).
1. [ ] People Experience: Enter the former team member's GitLab email address, handle, and Department below.

### BambooHR

1. [ ] The People Business Partner in charge of the offboarding will create a [workflow](https://about.gitlab.com/handbook/people-group/offboarding/#offboarding-slack-workflow) in the `#offboarding` slack channel, confirm the offboarding Type, offboarding Reason and if the team member is Eligible for Rehire.
1. [ ] If the team member is being placed on Garden Leave, update the Employment Status Table to reflect the date of garden leave starting.
1. [ ] At the far right corner of the team member's profile, click on the gear icon, choose `Terminate Employee`. Use the date mentioned in this issue as final date of employment / contract. Enter Termination Type, Termination Reason and Eligible for Rehire per the answers received from the People Partner.
1. [ ] If a team member is terminated before their Probationary Period has completed, delete the "Active" and "End of Probation Period" line dated in the future in the Employment Status section of BambooHR. The top entry of the Employment Status Table should be the "Termination" entry. 
1. [ ] Okta entitlements are driven based on BambooHR status. Therefore to remove the user in Okta, the best way to set off that workflow is to force an Import from BambooHR into Okta. To do this, log into the [Okta dashboard](https://gitlab-admin.okta.com/) and select `Admin` in the upper-right hand side of the page. Once on the Okta Admin dashboard, select `Applications`, search and select `BambooHR Admin`, go to the `Import` tab and click the `Import Now` button. This will force an import, and you should see a message that a user has been removed after processing completes. (This will not be the case if the team member is placed on Garden Leave)
1. [ ] If the team member is on **Garden Leave**, notify ITOps to manually deactive Okta. 

_Note: This will also trigger any deprovisioning workflows for Applications that Okta is configured to perform Deprovisioning on, as well as disable the user's Okta account._


### Notifications

1. [ ] Notify Security of the offboarding (@JohnathanHunt).
1. [ ] Notify Sr Stock Administrator (@tdominique) of offboarding. If this team member is on garden leave, please ensure the exact dates and process is shared with the Sr Stock Administrator.
1. [ ] Depending on the team members location, please ping the relevant payroll contact persons in this issue:
         - US and Canada: Ann Tiseo and Cristine Marquardt 
         - Non-US: Harley Devlin and Nicole Precilla
1. [ ] Scroll to the bottom of this issue to ensure there are not additonal People Experience Tasks for Department/Division/Entity/Role Task

## Complete below task before due date  

### Handbook

1. [ ] Team Page: 
    1. [ ] Remove team members image from the [team page](https://about.gitlab.com/team) by going to `/sites/marketing/source/images/team/` and finding the team member's photo.
    1. [ ] If the team member is a people manager or an interim people manager at GitLab, be sure to remove their slug from their direct reports profiles on the team page as well. If a new manager is not yet assigned, please replace the manager slug with the departing GitLab team member's direct manager. (I.E. If the departing team member with the title Manager, GitLab reports to Director, GitLab then Director, GitLab should be listed as the manager for Manager, GitLab's direct reports)
    1. [ ] Remove the team members entry on the team page by going to the [YML File](https://gitlab.com/gitlab-com/www-gitlab-com/-/blob/master/data/team.yml), selecting `WEB IDE`, navigate to `data/team.yml` and search for the team member. 
    1. [ ] Add this [issue link](https://docs.gitlab.com/ee/user/project/issues/crosslinking_issues.html#from-related-issues) to the Merge Request Description - This will link the issue and the merge request for easy visability. 
1. [ ] If applicable, remove team member's pet(s) from [team pets page](https://about.gitlab.com/team-pets). Don't forget to remove the picture(s).
       _Tip: Once on the [team pets page](https://about.gitlab.com/team-pets), you can check to see if the team member has a pet listed by right clicking, selecting `View Page Source`, and searching the team member's name._
1. [ ] Remove hard-coded reference links of the team member from our documentation and Handbook (i.e. links to the team page) by following the [instructions on the Handbook](https://about.gitlab.com/handbook/people-group/offboarding/offboarding_guidelines/)
1. [ ] Remove offboarding team members Read Me if applicable.


### Access 
1. [ ] Delete account in [Moo](https://moo.com).
1. [ ] Delete the team member from [HelloSign](https://app.hellosign.com/home/team)
1. [ ] Delete any pending invitiations to the team member [gitlab-com group](https://gitlab.com/groups/gitlab-com/-/group_members) and [gitlab-org group](https://gitlab.com/groups/gitlab-org/-/group_members).
1. [ ] For both voluntary and involuntary offboardings, confirm the former team member's eligibility to be added to the `gitlab-alumni` Slack channel with the relevant People Partner in the message thread if no confirmation is received in the Slack offboarding form. If the People Partner confirms eligibility, please open an [Access Request](https://gitlab.com/gitlab-com/team-member-epics/access-requests/-/issues/new?issue%5Bassignee_id%5D=&issue%5Bmilestone_id%5D=) issue to add team member as a single channel guest to `gitlab-alumni` with the personal email found in BambooHR.

### Last Steps
1. [ ] Confirm that a departure announcement has been made in #team-member-updates on Slack.


</details>

<details>
<summary>Total Rewards</summary>

1. [ ] Total Rewards Analyst (@julie.samson): Ensure there is no outstanding tuition reimbursement that would need to be refunded to GitLab.
1. [ ] Total Rewards Analyst (@SVBarry): (Canadian team members only) Remove from benefits.

</details>

<details>
<summary>Recruiting Operations</summary>

1. [ ] Recruiting Operations (@recruitingops): Deactivate former team member in [Greenhouse](https://app2.greenhouse.io/dashboard#).
   - @gitlab-com/gl-ces will complete their off-boarding tasks prior to the deactivation of the team member's Greenhouse account

</details>

## Business Technology

<details>
<summary>Business Systems Analyst</summary>

1. [ ] Business Systems Analyst (@lisvinueza): Review and confirm if team member is a system admin/provisioner.
    - [ ] Business Systems Analyst (@lisvinueza): If team member is a system admin/provisioner, open and complete an [Update Tech Stack Provisioner issue](https://gitlab.com/gitlab-com/team-member-epics/access-requests/issues/new?issuable_template=Update_Tech_Stack_Provisioner).
1. [ ] Business Systems Analyst (@lisvinueza): Review and remove any mention of the team member from the [offboarding tasks templates](https://gitlab.com/gitlab-com/people-group/employment-templates-2/-/tree/master/.gitlab/issue_templates/offboarding_tasks)
1. [ ] Business Systems Analyst (@lisvinueza): Review and remove any mention of the team member from the [onboarding tasks templates](https://gitlab.com/gitlab-com/people-group/employment-templates-2/-/tree/master/.gitlab/issue_templates/onboarding_tasks)
1. [ ] @PhilEncarnacion @awestbrook: Reach out to [TripActions Support](https://app.tripactions.com/app/user/search) to determine the best course of action for any pre-booked flights.

</details>

<details>
<summary>Team Member Enablement (IT)</summary>

1. [ ] IT Ops: Log on to the [Google Admin console](https://admin.google.com/gitlab.com/AdminHome?pli=1&fral=1#UserList:org=49bxu3w3zb11yx) and find the team member's profile.
1. [ ] IT Ops: Click the `More` button (left hand side of screen) and then click `Suspend User` and click it again to confirm. This will ensure that the user can no longer access their account.
1. [ ] IT Ops: Rename the team member's email from `username@gitlab.com` to `former_username@gitlab.com`
1. [ ] IT Ops: Select the dropdown icon `ˇ` in the `User information` section and delete the email alias associated with the user.
1. [ ] IT Ops: Following the [instructions in the Handbook](https://about.gitlab.com/handbook/people-group/offboarding/offboarding_guidelines/#g-suite), set an automatic rejection rule. This sends an auto-response to the sender notifying them that the team member is no longer with GitLab and who to contact instead.
1. [ ] IT Ops: As per the Manager's comment in this issue (ping them if they haven't commented yet), please transfer ownership of documents to the Manager, if so requested.
1. [ ] IT Ops @gitlab-com/business-ops/team-member-enablement : Check if former GitLab Team Member had a gold account on GitLab and if so, downgrade the account.
   - If needed, you can find who has a gold account by searching in the [ARs project](https://gitlab.com/gitlab-com/team-member-epics/access-requests/-/issues?scope=all&utf8=%E2%9C%93&state=closed&label_name[]=GitLab%20Gold%20Request) and the [Gold Entitlement Request project](https://gitlab.com/gitlab-com/team-member-epics/gold-entitlement-request/-/issues?scope=all&utf8=%E2%9C%93&state=all)) for Closed `~"GitLab Gold Request"`s. You can use the `author` filter to narrow it down if needed.
1. [ ] IT Ops @gitlab-com/business-ops/team-member-enablement : Remove former GitLab Team Member's' GitLab.com account from the [gitlab-com group](https://gitlab.com/groups/gitlab-com/-/group_members).
   1. [ ] IT Ops @gitlab-com/business-ops/team-member-enablement : Block former GitLab Team Member, remove from all company Groups and Projects, and remove GitLab job role if visible on profile.
   1. [ ] IT Ops @gitlab-com/business-ops/team-member-enablement : **IF** the former GitLab Team Member's start date was **prior to 2020-03-23**, change the primary email address on the GitLab.com account to be a personal email address of the former Team Member and unblock the account. 
   1. [ ] IT Ops @gitlab-com/business-ops/team-member-enablement : Remove former GitLabber's admin account, if applicable.
1. [ ] IT Ops @gitlab-com/business-ops/team-member-enablement : Block former GitLab Team Member's [dev.GitLab.org account](https://dev.gitlab.org/admin/users) and remove from [gitlab group](https://dev.gitlab.org/groups/gitlab/group_members).
1. [ ] IT Ops @gitlab-com/business-ops/team-member-enablement : Block former GitLab Team Member's [staging.GitLab.com account](https://staging.gitlab.com/admin/users) and remove from [gitlab group](https://staging.gitlab.com/groups/gitlab/group_members).
1. [ ] IT Ops: [Check if the team member has created any Slack bots](https://about.gitlab.com/handbook/people-group/offboarding/offboarding_guidelines/) before disabling account. [Link](https://gitlab.slack.com/apps/manage?utm_source=in-prod&utm_medium=inprod-apps_link-slack_menu-click) to Apps menu.
1. [ ] IT Ops: Disable team member in [Slack](https://gitlab.slack.com/admin). If the team member is an Admin ping @amberlammers or @pkaldis to remove them.
1. [ ] IT Ops: Okta should now deprovision 1Password as part of Okta Deactivation, please verify it was deactivated. If not, please ping the team in the #it-help Slack channel.
1. [ ] IT Ops: Reach out to former team member to identify and retrieve any company supplies/equipment. @gitlab-com/business-ops/team-member-enablement See the [Offboarding page](https://about.gitlab.com/handbook/people-group/offboarding/) for further details on that process.
1. [ ] IT Ops @gitlab-com/business-ops/team-member-enablement : Comment in this issue when equipment has been returned/recycled. Ping hiring manager, Security and `@Courtney_cote`.
1. [ ] IT Ops @gitlab-com/business-ops/team-member-enablement : Inform Controller / Accounting if any items in former GitLab Team Member's possession will not be returning, so that they can be removed from asset tracking.
1. [ ] IT Ops: Okta should now deprovision Zoom as part of Okta Deactivation, please verify it was deactivated. If not, please ping the team in the #it-help Slack channel.
1. [ ] IT Ops: After 5 days, when the former team member's offboarding issue is due, delete their GSuite account now referred to as `former_username@gitlab.com`. During this process, you will receive a prompt to transfer their Google Drive Documents. Please refer to the offboarded team member's manager task to see whether they have requested to have Document ownership transferred to them, or if they have agreed to not receive ownership. Delete the team members Google account 5 days after the offboarding date.


</details>

## Finance

<details>
<summary>Accounting</summary>

1. [ ] Accounting (@Courtney_cote @kayokocooper): Do not approve any submitted or in progress Expensify expenses until there is confirmation of return of laptop.
1. [ ] Accounting (@Courtney_cote @kayokocooper): Cancel company American Express card if applicable.
1. [ ] Accounting (@Courtney_cote @kayokocooper): Remove team member from Expensify.
1. [ ] Accounting (@Courtney_cote @kayokocooper): Reallocate team members direct reports if any within Expensify for pending and future approvals in alignment with direction provided by the relevant People Business Partner (PBP).

</details>

# Tech Stack System Deprovisioning / Offboarding - Tasks that need to be completed within 5 days

## System Admins


Review the table of applications listed below that is included in our [Tech Stack](https://docs.google.com/spreadsheets/d/1mTNZHsK3TWzQdeFqkITKA0pHADjuurv37XMuHv12hDU/edit#gid=0). For applications that you Own/Admin, please review and ensure the former team member does **not** have access to your system(s).

**If the user DOES have access to a system you administer access to:**

- Review and remove the former team member's access.
- Select the applicable checkbox(es) once access has been removed from the impacted system.

**If the user DOES NOT have access to a system you administer access to:**

- After confirming the former team member does not have access to the system, select the checkbox indicating access has been deprovisioned.


## Community Relations

#### <summary>Lindsay Olson @LindsayOlson </summary>


<table >
	<tbody>
		<tr>
			<td> Disqus  </td>
			<td>

- [ ] Access Deprovisioned
- [ ] Not Applicable to Team Member 

</td>
		</tr>
	</tbody>
</table>

<table >
	<tbody>
		<tr>
			<td> Zendesk - Community  </td>
			<td>

- [ ] Access Deprovisioned
- [ ] Not Applicable to Team Member 

</td>
		</tr>
	</tbody>
</table>

#### <summary>Liam McAndrew @lmcandrew</summary>
- Lower the team member's permissions on [Crowdin](https://translate.gitlab.com/project/gitlab/settings#members) to translator 

<table >
	<tbody>
		<tr>
			<td> CrowdIn </td>
			<td>

- [ ] Access Deprovisioned
- [ ] Not Applicable to Team Member 

</td>
		</tr>
	</tbody>
</table>

## Data

#### <summary>Taylor Murphy @tayloramurphy</summary>

<table >
	<tr>
			<td> Snowflake </td>
			<td>

- [ ] Access Deprovisioned
- [ ] Not Applicable to Team Member 

</td>
		</tr>
    <tr>
			<td> Stitch </td>
			<td>

- [ ] Access Deprovisioned
- [ ] Not Applicable to Team Member 

</td>
		</tr>
		<tr>
			<td> Fivetran </td>
			<td>

- [ ] Access Deprovisioned
- [ ] Not Applicable to Team Member 

</td>
		</tr>


</table>


## Development


#### <summary>Jean Du Plessis @jeanduplessis, Tim Zallmann @timzallmann</summary>

<table >
	<tbody>
		<tr>
			<td> frontendmasters.com  </td>
			<td>

- [ ] Access Deprovisioned
- [ ] Not Applicable to Team Member 

</td>
		</tr>
	</tbody>
</table>

## Team member Enablement (IT)

#### <summary>Amber Lammers @amberlammers, Mohammed Al Kobaisy @malkobaisy </summary>

<table >
	<tbody>
		<tr>
			<td> PagerDuty  </td>
			<td>

- [ ] Access Deprovisioned
- [ ] Not Applicable to Team Member 

</td>
		</tr>
	</tbody>
</table>


#### <summary> @gitlab-com/business-ops/team-member-enablement </summary>

<table >
	<tbody>
		<tr>
			<td> LucidChart  </td>
			<td>

- [ ] Access Deprovisioned
- [ ] Not Applicable to Team Member 

</td>
		</tr>
	</tbody>
</table>

## Infrastructure

#### <summary>David Smith @dawsmith, Anthony Sandoval @AnthonySandoval </summary>

<table >
  <tbody>
		<tr>
			<td> AWS (Production)  </td>
			<td>

- [ ] Access Deprovisioned
- [ ] Not Applicable to Team Member 

</td>
		</tr>
		<tr>
			<td> AWS (Support / Staging)  </td>
			<td>

- [ ] Access Deprovisioned
- [ ] Not Applicable to Team Member 

</td>
		</tr>
		<tr>
			<td> AWS (Gov Cloud)  </td>
			<td>

- [ ] Access Deprovisioned
- [ ] Not Applicable to Team Member 

</td>
		</tr>
		<tr>
			<td> AWS (GitLab.io)  </td>
			<td>

- [ ] Access Deprovisioned
- [ ] Not Applicable to Team Member 

</td>
		</tr>
		<tr>
			<td> Azure  </td>
			<td>

- [ ] Access Deprovisioned
- [ ] Not Applicable to Team Member 

</td>
		</tr>
		<tr>
			<td> Chef  </td>
			<td>

- [ ] Access Deprovisioned
- [ ] Not Applicable to Team Member 

</td>
		</tr>
		<tr>
			<td> CloudFare  </td>
			<td>

- [ ] Access Deprovisioned
- [ ] Not Applicable to Team Member 

</td>
		</tr>
		<tr>
			<td> Digital Ocean (GitLab BV)  </td>
			<td>

- [ ] Access Deprovisioned
- [ ] Not Applicable to Team Member 

</td>
		</tr>
		<tr>
			<td> Digital Ocean (GitLab CI Runners)  </td>
			<td>

- [ ] Access Deprovisioned
- [ ] Not Applicable to Team Member 

</td>
		</tr>
		<tr>
			<td> Digital Ocean (GitLab Dev)  </td>
			<td>

- [ ] Access Deprovisioned
- [ ] Not Applicable to Team Member 

</td>
		</tr>
		<tr>
			<td> Digital Ocean (GitLab GitHost)  </td>
			<td>

- [ ] Access Deprovisioned
- [ ] Not Applicable to Team Member 

</td>
		</tr>
		<tr>
			<td> Digital Ocean (GitLab Prod)  </td>
			<td>

- [ ] Access Deprovisioned
- [ ] Not Applicable to Team Member 

</td>
		</tr>
		<tr>
			<td> Elastic Cloud  </td>
			<td>

- [ ] Access Deprovisioned
- [ ] Not Applicable to Team Member 

</td>
		</tr>
		<tr>
			<td> Fastly CDN  </td>
			<td>

- [ ] Access Deprovisioned
- [ ] Not Applicable to Team Member 

</td>
		</tr>
		<tr>
			<td> GitLab.com Prod/staging rails and db console (ssh)  </td>
			<td>

- [ ] Access Deprovisioned
- [ ] Not Applicable to Team Member 

</td>
		</tr>
		<tr>
			<td> customers. -gitlab.com (ssh)  </td>
			<td>

- [ ] Access Deprovisioned
- [ ] Not Applicable to Team Member 

</td>
		</tr>
		<tr>
			<td> license. -gitlab.com (ssh)  </td>
			<td>

- [ ] Access Deprovisioned
- [ ] Not Applicable to Team Member 

</td>
		</tr>
		<tr>
			<td> version. -gitlab.com (ssh)  </td>
			<td>

- [ ] Access Deprovisioned
- [ ] Not Applicable to Team Member 

</td>
		</tr>	
		<tr>
			<td> ops.gitlab.net  </td>
			<td>

- [ ] Access Deprovisioned
- [ ] Not Applicable to Team Member 

</td>
		</tr>	
		<tr>
			<td> Grafana (dashboards.gitlab.net)  </td>
			<td>

- [ ] Access Deprovisioned
- [ ] Not Applicable to Team Member 

</td>
		</tr>
		<tr>
			<td> Sentry (sentry.gitlab.net)  </td>
			<td>

- [ ] Access Deprovisioned
- [ ] Not Applicable to Team Member 

</td>
		</tr>
		<tr>
			<td> PackageCloud  </td>
			<td>

- [ ] Access Deprovisioned
- [ ] Not Applicable to Team Member 

</td>
		</tr>	
		<tr>
			<td> Rackspace  </td>
			<td>

- [ ] Access Deprovisioned
- [ ] Not Applicable to Team Member 

</td>
		</tr>
		<tr>
			<td> Status - IO  </td>
            <td>

- [ ] Access Deprovisioned
- [ ] Not Applicable to Team Member 

</td>
		</tr>
  </tbody>
</table>


## Finance

#### <summary> Finance Ops/Finance System Admins @gitlab-com/business-ops/enterprise-apps/financeops </summary>

<table >
	<tbody>
		<tr>
			<td> Stripe  </td>
			<td>

- [ ] Access Deprovisioned
- [ ] Not Applicable to Team Member 

</td>
		</tr>
		<tr>
			<td> Netsuite  </td>
			<td>

- [ ] Access Deprovisioned
- [ ] Not Applicable to Team Member 

</td>
		</tr>
		<tr>
			<td> Zuora  </td>
			<td>

- [ ] Access Deprovisioned
- [ ] Not Applicable to Team Member 

</td>
		</tr>
		<tr>
			<td> Tipalti  </td>
			<td>

- [ ] Access Deprovisioned
- [ ] Not Applicable to Team Member 

</td>
		</tr>
	</tbody>
</table>



#### <summary>Toni Dominique @tdominique</summary>

<table >
	<tbody>
		<tr>
			<td> Carta  </td>
			<td>

- [ ] Access Deprovisioned
- [ ] Not Applicable to Team Member 

</td>
		</tr>
	</tbody>
</table>

## Legal

#### <summary>Robert Nalen @rnalen, Taharah Nix @tnix </summary>

<table >
	<tbody>
		<tr>
			<td> ContractWorks  </td>
			<td>

- [ ] Access Deprovisioned
- [ ] Not Applicable to Team Member 

</td>
		</tr>
		<tr>
			<td> Visual Compliance  </td>
			<td>

- [ ] Access Deprovisioned
- [ ] Not Applicable to Team Member 

</td>
		</tr>
		<tr>
			<td> Navex  </td>
			<td>

- [ ] Access Deprovisioned
- [ ] Not Applicable to Team Member

</td>
		</tr>
	</tbody>
</table>

## Marketing

#### <summary>Aricka Flowers @atflowers, Erica Lindberg @erica </summary>

<table >
	<tbody>
		<tr>
			<td> YouTube  </td>
			<td>

- [ ] Access Deprovisioned
- [ ] Not Applicable to Team Member 

</td>
		</tr>
	</tbody>
</table>

#### <summary>Brandon Lyon @brandon_lyon </summary>

<table >
	<tbody>
		<tr>
			<td> LaunchDarkly  </td>
			<td>

- [ ] Access Deprovisioned
- [ ] Not Applicable to Team Member 

</td>
		</tr>
	</tbody>
</table>

#### <summary>Dara Warde @darawarde </summary>

<table >
	<tbody>
		<tr>
			<td> Mandrill  </td>
			<td>

- [ ] Access Deprovisioned
- [ ] Not Applicable to Team Member 

</td>
		</tr>
		<tr>
			<td> Mailchimp  </td>
			<td>

- [ ] Access Deprovisioned
- [ ] Not Applicable to Team Member 

</td>
		</tr>
	</tbody>
</table>



#### <summary>Ginny Reib @greib</summary>

<table >
	<tbody>
		<tr>
			<td> Alyce  </td>
			<td>

- [ ] Access Deprovisioned
- [ ] Not Applicable to Team Member 

</td>
		</tr>
	</tbody>
</table>

#### <summary>Jameson Burton @jburton </summary>

<table >
	<tbody>
		</tr>
		<tr>
			<td> LinkedIn Sales Navigator  </td>
			<td>

- [ ] Access Deprovisioned
- [ ] Not Applicable to Team Member 

</td>
		</tr>
		<tr>
			<td> Marketo  </td>
			<td>

- [ ] Access Deprovisioned
- [ ] Not Applicable to Team Member 

</td>
		</tr>
	</tbody>
</table>

#### <summary>Robert Kohnke @rkohnke, Shane Rice @shanerice </summary>

<table >
	<tbody>
		<tr>
			<td> Google Analytics  </td>
			<td>

- [ ] Access Deprovisioned
- [ ] Not Applicable to Team Member 

</td>
		</tr>
		<tr>
			<td> Google Tag Manager  </td>
			<td>

- [ ] Access Deprovisioned
- [ ] Not Applicable to Team Member 

</td>
		</tr>
		<tr>
			<td> Google Adwords  </td>
			<td>

- [ ] Access Deprovisioned
- [ ] Not Applicable to Team Member 

</td>
		</tr>
	</tbody>
</table>

## People

#### <summary>Recruiting Operations</summary>

<table >
	<tbody>
		<tr>
			<td> ContactOut (Sourcing Team Only)  </td>
			<td>

- [ ] Access Deprovisioned
- [ ] Not Applicable to Team Member 

</td>
		</tr>
		<tr>
			<td> DocuSign (@ewegscheider)  </td>
			<td>

- [ ] Access Deprovisioned
- [ ] Not Applicable to Team Member 

</td>
		</tr>
		<tr>
			<td> LinkedIn Recruiter  </td>
			<td>

- [ ] Access Deprovisioned
- [ ] Not Applicable to Team Member 

</td>
		</tr>
	</tbody>
</table>
- If the deactivated team member is a member of the Recruiting Team, please reassign their seat to their Manager.

    - Click Edit > Reassign seat > "... click here to reassign by a member's name" > Reassign

<summary>@gitlab-com/gl-ces</summary>

- If Recruiting Ops has already completed their steps:

1. [ ] Search and Update the Hiring Repo, if applicable.
1. [ ] Update the Guide Interviewers.

- If Recruiting Ops has not completed their steps:

1. [ ] Search for any currently scheduled Interviews in the Google Interview Calendar by searching for the team member's name in the Google Calendar Search Bar. If applicable, assign the interviews to another team member in Greenhouse and notify the recruiter.
1. [ ] Check Interview Plans and requisition Hiring Manager fields for the reqs assigned to the team member by searching the team member’s name under Users in Greenhouse and update plans and details accordingly.
1. [ ] Post in the recruiting-team-ces channel the departing team member's name and list reqs interviews were currently scheduled for. Advise the rest of the CES team to review any other interview plans and update accordingly if applicable.
1. [ ] Search and Update the Hiring Repo, if applicable. 
1. [ ] Update the Guide Interviewers.
1. [ ] Notify @recruitingops upon completion of tasks.


#### <summary>Learning and Development @jbandur & @slee24</summary>

<table >
	<tbody>
		<tr>
			<td> Will Learning  </td>
			<td>

- [ ] Access Deprovisioned
- [ ] Not Applicable to Team Member 

</td>
		</tr>
	</tbody>
</table>

#### <summary>People Engineering @lienvdsteen</summary>
<table >
	<tbody>
		<tr>
			<td> Assessment Tool </td>
			<td>

- [ ] Access Deprovisioned
- [ ] Not Applicable to Team Member 

</td>
		</tr>
	</tbody>
</table>

## Sales

### <summary>Amber Stahn @Astahn, Meri Gil Galindo @mgilgalindo</summary>

<table >
	<tbody>
		<tr>
			<td> Datafox  </td>
			<td>

- [ ] Access Deprovisioned
- [ ] Not Applicable to Team Member 

</td>
		</tr>
		<tr>
			<td> Salesforce  </td>
			<td>

- [ ] Access Deprovisioned
- [ ] Not Applicable to Team Member 

</td>
		</tr>
		<tr>
			<td> Salesforce - Reference Edge  </td>
			<td>

- [ ] Access Deprovisioned
- [ ] Not Applicable to Team Member 

</td>
		</tr>
		<tr>
			<td> Salesforce - Gainsight  </td>
			<td>

- [ ] Access Deprovisioned
- [ ] Not Applicable to Team Member 

</td>
		</tr>
	</tbody>
</table>

<table >
	<tbody>
		<tr>
			<td> Clari  </td>
			<td>

- [ ] Access Deprovisioned
- [ ] Not Applicable to Team Member 

</td>
		</tr>
		<tr>
			<td> Chorus  </td>
			<td>

- [ ] Access Deprovisioned
- [ ] Not Applicable to Team Member 

</td>
		</tr>
	</tbody>
</table>

### <summary>Caitlin Ankney @cankney</summary>

<table >
	<tbody>
		<tr>
			<td> Gainsight  </td>
			<td>

- [ ] Access Deprovisioned
- [ ] Not Applicable to Team Member 

</td>
		</tr>
	</tbody>
</table>

#### <summary>Lisa Puzar @lisapuzar Swetha Kashyap @Swethakashyap </summary>

<table >
	<tbody>
		<tr>
			<td> CaptivateIQ  </td>
			<td>

- [ ] Access Deprovisioned
- [ ] Not Applicable to Team Member 

</td>
		</tr>
	</tbody>
</table>


## Security

#### <summary>Anthony Carella @acarella </summary>

<table >
	<tbody>
		<tr>
			<td> Tenable.IO  </td>
			<td>

- [ ] Access Deprovisioned
- [ ] Not Applicable to Team Member 

</td>
		</tr>
	</tbody>
</table>

#### <summary>James Ritchey @jritchey, Ethan Strike @estrike </summary>

<table >
	<tbody>
		<tr>
			<td> HackerOne  </td>
			<td>

- [ ] Access Deprovisioned
- [ ] Not Applicable to Team Member 

</td>
		</tr>
	</tbody>
</table>

#### <summary>Julia Lake @Julia.Lake </summary>

<table >
	<tbody>
		<tr>
			<td> ZenGRC  </td>
			<td>

- [ ] Access Deprovisioned
- [ ] Not Applicable to Team Member 

</td>
		</tr>
	</tbody>
</table>

#### <summary>Kyle Smith @kyletsmith</summary>

<table >
	<tbody>
		<tr>
			<td> Rackspace (Security Enclave)  </td>
			<td>

- [ ] Access Deprovisioned
- [ ] Not Applicable to Team Member 

</td>
		</tr>
		<tr>
			<td> AWS Security  </td>
			<td>

- [ ] Access Deprovisioned
- [ ] Not Applicable to Team Member 

</td>
		</tr>
		<tr>
			<td> Panther SIEM  </td>
			<td>

- [ ] Access Deprovisioned
- [ ] Not Applicable to Team Member 

</td>
		</tr>
	</tbody>
</table>


## Support

#### <summary>Jason Colyer @jcolyer </summary>

<table >
	<tbody>
		<tr>
			<td> Calendly Premium  </td>
			<td>

- [ ] Access Deprovisioned
- [ ] Not Applicable to Team Member 


	</tbody>
</table>

#### <summary>Nabeel Bilgrami @nabeel.bilgrami </summary>

<table >
	<tbody>
		<tr>
			<td> Zendesk Light Agent  </td>
			<td>

- [ ] Access Deprovisioned
- [ ] Not Applicable to Team Member 

</td>
		</tr>
		<tr>
			<td> Zendesk - Remove the agents 'user tags'  </td>
			<td>

- [ ] Access Deprovisioned
- [ ] Not Applicable to Team Member 

</td>
		</tr>
	</tbody>
</table>

## UX

#### <summary>Adam Smolinski @asmolinski2, Jeff Crow @jeffcrow </summary>

<table >
	<tbody>
		<tr>
			<td> Qualtrics  </td>
			<td>

- [ ] Access Deprovisioned
- [ ] Not Applicable to Team Member 

</td>
		</tr>
	</tbody>
</table>


---

# Job Specific Offboarding Task  
If a task only applies to a Entity, Country, Division, Department or Role it will appear under here. 

<!-- include: entity_tasks -->

---

<!-- include: country_tasks -->

---

<!-- include: division_tasks -->

---

<!-- include: department_tasks -->

---

<!-- include: people_manager_tasks -->
