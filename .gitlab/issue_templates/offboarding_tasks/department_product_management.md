### FOR PRODUCT MANAGEMENT

1. [ ] @clenneville @vkarnes: Remove former team member from [Balsamiq Cloud](https://balsamiq.cloud)
1. [ ] Manager: Remove former team member from [Mural](https://www.mural.co/)
1. [ ] @asmolinski2 @loriewhitaker: Remove former team member from [Dovetail]

