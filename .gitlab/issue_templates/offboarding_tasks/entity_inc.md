### For GitLab Inc team members only

People Experience

1. [ ] People Experience: Notify payroll specialist (@atiseo @csotomango) of offboarding.

Payroll / Finance Ann Tiseo @atiseo, Sara Honeycutt @shoneycutt

1. [ ] Deprovision the team member from ADP

