### FOR MARKETING ONLY

* [ ] Marketing Ops: Remove from Tweetdeck.

<summary>Sales/Marketing Ops</summary>

1. [ ] Sales/Marketing Ops: Disable User and Other Actions
      
      * [ ]   Salesforce - @@tav_scott: REASSIGN RECORDS in Salesforce: Export all leads, accounts, contacts, and open opportunities OWNED by the former team member and save as a flat file or googlesheets. Name the file `LASTNAME-FIRSTNAME-OBJECT-YYYY-MM-DD` (Object will be LEADS, ACCOUNTS, CONTACTS, or OPPORTUNITIES). Then reassign all leads, accounts, contacts, and open opportunities (do Not Reassign CLOSED WON/LOST OPPS!) to their Manager.
      * [ ]   Salesforce - @Astahn or @mgilgalindo: REMOVE FROM PICKLISTS: remove from SDR or BDR picklists on ACCOUNT and OPPORTUNITY objects and if applicable, replace the former SDR/BDR to new SDR/BDR on both the account and opportunity objects. Please do not replace the SDR/BDR CLOSED WON/LOST OPPS and only replace opportunities with a new SDR/BDR if the opportunity is not yet qualified

      * [ ]   Outreach - @jburton: First, lock the user, Second, turn off both "Enable sending messages from this mailbox" and "Enable syncing messages from this mailbox".
      * [ ]   ZoomInfo - @jburton: Deactivate and remove former team member from ZoomInfo.
      * [ ]   Marketo - @jburton: Remove from Workflow Campaigns, pick lists, and SFDC assignment sync.
      * [ ]   LinkedIn Sales Navigator - @jburton: Disable user, remove from team license.
      * [ ]   Drift - @jburton: IF User: Disable access; IF Admin: Reset password.
      * [ ]   LeanData - @bethpeterson @jburton: Remove from any lead routing rules and round robin groups.
      * [ ]   1Password - @cbeer: Rotate any shared login credentials (SFDC API user, Google Analytics, Adwords, Disqus, Facebook, LinkedIn, Zendesk).
      * [ ]   Ringlead - Melinda Soares @msoares1: Remove team member from Ringlead if applicable.
      * [ ]   Demandbase - Emily Luehrs @emilyluehrs: Remove team member from Demandbase if applicable. 
      * [ ]   ARInsights' Architect - Ryan Ragozzine @rragozzine: Remove team member from ARInsights' Architect if applicable.
      * [ ]   Sigstr @darawarde: Remove team member from Sigstr.
      * [ ]   PathFactory @jburton: Remove team member from PathFactory.
      * [ ]   HotJar @jburton: Remove team member from HotJar.
      * [ ]   Litmus @sdaily: Remove team member from Litmus.
      * [ ]   Facebook Ad Platform @mnguyen4: Remove team member from Facebook Ad Platform.
      * [ ]   Google Search Console @darawarde @sdaily @shanerice: Remove the team member from Google Search Console
      * [ ]   Shopify @darawarde: Remove the team member from Shopify

1. [ ] @wspillane Remove Sprout Social Access
1. [ ] @wspillane Remove Tweetdeck Access
1. [ ] IMPartner - @ecepulis @KJaeger: Remove team member from IMPartner if applicable.


