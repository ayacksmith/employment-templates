### FOR INFRASTRUCTURE TEAM MEMBERS ONLY

- [ ] @acarella: Remove the team member from Tenable.IO
- [ ] @dawsmith @AnthonySandoval @albertoramos: Remove the team member from Chef
- [ ] @dawsmith @AnthonySandoval @albertoramos: Remove the team member from Cloudflare
- [ ] @dawsmith @AnthonySandoval @albertoramos: Remove the team member from Fastly CDN
- [ ] @dawsmith @AnthonySandoval @albertoramos: Remove the team member from PackageCloud
- [ ] @dawsmith @AnthonySandoval @albertoramos: Remove the team member from Status - IO
- [ ]   Google Search Console @darawarde @sdaily: Remove the team member from Google Search Console

