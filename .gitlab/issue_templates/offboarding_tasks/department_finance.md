<summary>Finance</summary>

1. [ ] Finance: Remove from Comerica, Rabobank or any other account (as user or viewer only if in Finance).
1. [ ] @mmaradiaga: Remove team member from Workiva and FloQast
1. [ ] Igor Groenewegen-Mackintosh @igroenewegenmackintosh: Remove team member from VATit SaaS
1. [ ] Igor Groenewegen-Mackintosh @igroenewegenmackintosh: Remove team member from Avalara

<summary>Manager</summary>

1. [ ] Manager: Remove from sales meeting.
