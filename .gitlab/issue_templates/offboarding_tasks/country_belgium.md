### For GitLab BV Belgium only

<summary>People Ops</summary>

1. [ ] People Ops: Once the termination date is known verify with COO or CEO that the non-competition clause is to be waived or enforced.
1. [ ] Non-competition waived: People Ops: send a letter (in French or Dutch depending on location in Belgium) via registered mail and by email to the team member within 15 days of the termination date.
1. [ ] Non-competition enforced: People Ops: to inform Financial Controller & instruct payroll to pay the team member a lump sum as stated in the contract.
1. [ ] People Ops: Inform payroll of last day and ask them to confirm how many ecocheques the person is due and when they will be issued. Payroll will also issue a vacation certificate which is required to be given to the next employer.