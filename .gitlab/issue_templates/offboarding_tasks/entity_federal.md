### For GitLab Federal team members only

People Experience

1. [ ] People Experience: Notify payroll specialist (@atiseo @csotomango) of offboarding.

Jason Colyer, (US) Support Managers @jcolyer

1. [ ] Zendesk - US Federal

Brent Caldwell @bcaldwell-gitlab

1. [ ] GovWin IQ

Payroll / Finance Ann Tiseo @atiseo, Cristine Marquardt @csotomango 

1. [ ] Deprovision the team member from ADP
