### FOR SALES ONLY

<summary>Sales/Marketing Ops</summary>

1. [ ] Sales/Marketing Ops: Disable User and Other Actions
      
      * [ ] @Astahn @mgilgalindo: Update Sale Operations Sponsored Reports/Dashbaords listed on [The Sales Operations Handbook Page](https://about.gitlab.com/handbook/sales/field-operations/sales-operations/#sales-operations-sponsored-dashboards-and-maintenance)
      * [ ] @msoares1 Salesforce Accounts and Opportunities: REASSIGN all accounts and open opportunities (do Not Reassign CLOSED WON/LOST OPPS!) based on direction provided by RD/VP within 24 hours of termination. Any named accounts will be transitioned either to RD or designated SAL/AM.
      * [ ] @msoares1: Update Territory Model in LeanData with temporary territory assignments and Sales Territories in Handbook for SALs/AEs.
      * [ ] @bethpeterson Salesforce Leads and contacts: REASSIGN all leads and contacts based on direction provided by SDR leadership within 24 hours of termination. 
      * [ ] @bethpeterson: Update Territory Model in LeanData with temporary territory assignments for SDRs.
      * [ ] Outreach - @jburton: First, lock the user, Second, turn off both "Enable sending messages from this mailbox" and "Enable syncing messages from this mailbox".
      * [ ] ZoomInfo - @jburton: Deactivate and remove former team member from ZoomInfo
      * [ ] LeanData - @bethpeterson: Remove from any lead routing rules and round robin groups.
      * [ ] LinkedIn Sales Navigator - @jburton Disable user, remove from team license.
1. [ ] Rubén or Oswaldo: Remove from admin panel in the [Subscription portal](https://customers.gitlab.com/admin).
1. [ ] Ringlead - Melinda Soares @msoares1: Remove team member from Ringlead if applicable.
1. [ ] @darawarde: Remove team member from Sigstr
1. [ ] IMPartner - @ecepulis @KJaeger: Remove team member from IMPartner if applicable.
