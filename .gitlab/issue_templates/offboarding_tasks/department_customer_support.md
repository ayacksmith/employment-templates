### FOR SUPPORT ENGINEERING/AGENTS ONLY

<summary>Manager</summary>

1. [ ] Zendesk [(general information about removing agents)](https://support.zendesk.com/hc/en-us/articles/203661156-Best-practices-for-removing-agents#2):
   1. [ ] Manager: Remove any triggers related to the agent - https://gitlab.zendesk.com/agent/admin/triggers.
   1. [ ] Manager: Downgrade the agent role to "end-user" - [more information](https://support.zendesk.com/hc/en-us/articles/203661156-Best-practices-for-removing-agents#2).
        + **Warning: This will unassign all tickets from the agent** Consider reducing the "full agent" count on our Zendesk license.
   1. [ ] Manager: Schedule a date to suspend the agent account. [More information](https://support.zendesk.com/hc/en-us/articles/203661156-Best-practices-for-removing-agents#3).
   1. [ ] Manager: Remove team member from [GitHub Support org](https://github.com/GitLab-Support), typically if they are also a ZenDesk admin.
1. [ ] Community Forum:
   1. [ ] Manager: Remove team member from "moderators" group on the [GitLab community forum](https://forum.gitlab.com/).
1. [ ] Hiring Manager: Downgrade GitHost.io account to user privileges - [Set `user_type` to `0`](https://dev.gitlab.org/gitlab/GitHost#create-a-new-admin-user).
1. [ ] @samdbeckham: Revoke JetBrains licenses. Go to the [user management](https://account.jetbrains.com/login) and search for the team member, revoke their licenses.
1. [ ] @dawsmith @AnthonySandoval: Remove the team member from Status - IO


<summary>IT Ops</summary>

1. [ ] IT Ops @gitlab-com/business-ops/team-member-enablement : Remove any development VMs. Send a merge request to [the dev-resources repo](https://gitlab.com/gitlab-com/dev-resources) to remove `dev-resources/name-surname.tf`. Follow the instructions [here](https://gitlab.com/gitlab-com/dev-resources/tree/master/dev-resources#how-do-i-delete-an-instance-i-dont-need-anymore).
1. [ ] IT Ops @gitlab-com/business-ops/team-member-enablement : Revoke GitLab.com admin access if they had it. If you're not sure (i.e. their access predates access requests) ping the manager.
