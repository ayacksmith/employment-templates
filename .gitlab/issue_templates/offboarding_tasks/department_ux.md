### FOR UX RESEARCHERS & PRODUCT DESIGNERS

<summary>Manager</summary>

1. [ ] Manager: Remove former team member from [Balsamiq Cloud](https://balsamiq.cloud)
1. [ ] Manager: Remove former team member's `Master` access to the [gitlab-design](https://gitlab.com/gitlab-org/gitlab-design) project on GitLab.com.
1. [ ] Manager: Remove former team member from [Mural](https://www.mural.co/)
1. [ ] @asmolinski2 or @loriewhitaker: Remove former team member from [Dovetail]
1. [ ] @asmolinski2 or @jeffcrow: Remove former team member from [Qualtrics](https://qualtrics.com)
<summary>IT Ops</summary>

1. [ ] IT Ops @gitlab-com/business-ops/team-member-enablement : remove team member from the `@uxers` User Group on Slack.

<summary>Other</summary>

1. [ ] @tauriedavis or @jeldergl: Remove former team member from [Figma]
