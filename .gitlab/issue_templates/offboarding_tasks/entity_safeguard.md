### For SafeGuard Employees

<summary>People Experience</summary>

1. [ ] People Experience: For voluntary terms, email the country specific SafeGuard representative and the Safeguard account manager of the last day of employment. In the email include the resignation email or letter from the team member. This should be saved in BambooHR in the termination folder. 
