#### People Managers

<details>
<summary>Team Member</summary>

1. [ ] Team Member: Review [Git page update](https://about.gitlab.com/handbook/git-page-update/) to learn more about contributing and using GitLab.
1. [ ] Team Member: Make a meaningful [update to the handbook](https://about.gitlab.com/handbook/handbook-usage/) and assign the merge request to your manager.
1. [ ] Team Member: If applicable, review the [Vacancy Creation Process](https://about.gitlab.com/handbook/hiring/vacancies/), to learn how to open a new position.
1. [ ] Team Member: Review the [Leadership handbook page](https://about.gitlab.com/handbook/leadership/), particularly the recommended [articles](https://about.gitlab.com/handbook/leadership/#articles) and [books](https://about.gitlab.com/handbook/leadership/#books).

</details>

<details>
<summary>Manager</summary>

1. [ ] Manager: Give member `Maintainer` access on [www-gitlab-com](https://gitlab.com/gitlab-com/www-gitlab-com).
1. [ ] Manager: Check if a new [becoming a gitlab manager](https://gitlab.com/gitlab-com/people-group/Training/-/issues/new?issuable_template=becoming-a-gitlab-manager) issue exists and is assigned to the team member.  This lives in the [training issues](https://gitlab.com/gitlab-com/people-ops/Training/issues) project and should be created automatically with the career mobility issue.  Provide a link to the issue in a comment below this onboarding checklist.
1. [ ] Manager:  Before the team member's effective date, update the reporting structure in BambooHR by completing the following steps:
   * Login into BambooHR and click the employees tab
   * Select the applicable employee(s)
   * If you cannot see the employees in that view, search for them in the Search bar
   * Once in the employee profile, on the upper right hand side, click the request a change drop down.
   * Select job information, and complete the updated fields, including the 'reports to' field.
   * People Ops will process the request and it will be updated shortly.
1. [ ] Manager: Before the effective date, update the reporting structure on the team page.
1. [ ] Manager: Comment in the issue if the team member needs an Interview Training Issue and/or additional permissions in Greenhouse.
1. [ ] Manager: Submit an [Access Request](https://gitlab.com/gitlab-com/team-member-epics/access-requests/issues/new?issuable_template=slack_googlegroup_1Passwordgroupvault) to have the team member added to the Manager Google Group.

</details>

<details>
<summary>People Experience</summary>

1. [ ] People Experience : If applicable, ensure that the automation has opened an [Interview Training Issue](https://gitlab.com/gitlab-com/people-ops/Training/blob/master/.gitlab/issue_templates/interview_training.md) and a [Becoming a GitLab Manager](https://gitlab.com/gitlab-com/people-group/Training/-/blob/master/.gitlab/issue_templates/becoming-a-gitlab-manager.md) in the [People Ops Training project](https://gitlab.com/gitlab-com/people-ops/Training/issues) and that it is assigned to the team member.
1. [ ] Recruitings Operations & Insights (@gl-recruitingops) : If applicable, [upgrade team member in Greenhouse to either "Interviewer" or "Job Admin: Hiring Manager"](https://about.gitlab.com/handbook/hiring/greenhouse/#access-levels-and-permissions) for any roles they will be interviewing for or a hiring manager for. If they will be opening new vacancies in the future, they will need to have the permission "Can create new jobs and request approvals" enabled. If they are an executive, ping Recruiting in order to add them to the Greenhouse vacancy and offer approval flow for their division.

</details>
