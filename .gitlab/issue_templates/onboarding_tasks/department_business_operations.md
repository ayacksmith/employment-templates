#### Business Operations 

<details>
<summary>Manager</summary>

1. [ ] Invite to meetings: 
    *  [ ]  All Roles: Monthly Finance, bi-weekly Business Technology meeting
    *  [ ]  Enterprise Applications Team: Invite to Weekly sync, Daily office hours
    *  [ ]  Team Member Enablement (IT) Team: Weekly meeting

</details>

<details>
<summary>New Team Member</summary>

1. [ ] Join Slack channels: #finance, #business-technology, #bt-team-lounge. Ask your manager to invite you to #bt-confidential (private channel)
    * [ ]  If you are a BSA: #bt-business-engagement. You can reach out in #bt-business-engagement to the other BSAs to ask which channels you should join based on your area of focus
    * [ ]  If you are on the Data Team: #data, #data-lounge. Find other channels to join in the [Data Handbook](https://about.gitlab.com/handbook/business-ops/data-team/#slack).
    * [ ]  If you are on the IT Team: #it_help. Ask your manager to invite you to any private channels.
    * [ ]  If you are in Procurement: #procurement

</details>