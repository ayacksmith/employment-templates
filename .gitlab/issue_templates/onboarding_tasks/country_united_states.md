### Day 1 - For New Team Members in the US 

<details>
<summary>New Team Member</summary>

1. [ ] New team member: Please be sure to enter your full Social Security Number as your National Identification Number in BambooHR on Day 1. If you do not, we will not be able to add you to payroll until it is entered.
1. [ ] New team member: If you have not done so before Day 1, make sure you have completed Section 1 of your I-9 in Fragomen (following instructions sent to your personal email address before your start date) by creating an account using the following link https://gitlab.i9servicecenter.com. Note - this is critical and must be completed on or before your first day of hire. You must contact People Ops if you have difficulty with this form. Send the name and agent of your Designated Agent for Section 2 to People Ops.
1. [ ] New team member (California residents only): Read and be aware of the [Victims of Domestic Violence Leave Notice](https://www.dir.ca.gov/dlse/Victims_of_Domestic_Violence_Leave_Notice.pdf?utm_campaign=17-Q3-AUG-ACC-PAS-SOI-CLIENT-CA%20DOMESTIC%20VIOLENCE%20EMPLOYMENT%20LEAVE%20ACT&utm_medium=email&utm_source=Eloqua&elqTrackId=d6bf3067231c4a75a25cfd2b11703199&elq=164aa89072ad408db913259dae224863&elqaid=10312&elqat=1&elqCampaignId=).
1. [ ] New team member: Review the [Labor and Employment Posters](https://about.gitlab.com/handbook/labor-and-employment-notices/) handbook page 

**To Do after I-9 is complete:**
1. [ ] New team member: Your invitation to ADP will arrive between Day 2-4 most probably as new Labbers are frequently added to ADP in waves coinciding with the ending of each pay period. Once you receive the login from ADP to your GitLab email address, update all information in ADP including Direct Deposit, W4 Withholdings, marital status, etc. Payroll will reach out to you prior to your first paycheck. 
1. [ ] New team member: Your invitation to Lumity will arrive between Day 2-4. Once you receive your invitation, elect your [benefits through Lumity](https://about.gitlab.com/handbook/total-rewards/benefits/general-and-entity-benefits/inc-benefits-us/). Your benefit choices will be retroactive to your start date. If you have any questions please reach out to Lumity at 844-2-LUMITY. If your questions are sensitive, please contact the [Total Rewards Team](https://about.gitlab.com/handbook/people-group/#how-to-reach-the-right-member-of-the-people-group).  
    1. [ ] New team member: For more information on our plans, please review the [Summary Plan Description](https://drive.google.com/file/d/1K8uybZ_pQc-XxpOaIEqnSN-UlvZCsf1W/view?usp=sharing).
    1. [ ] New team member: Please also ensure you elect into your benefits for 2021 as well as your benefits for 2020 since GitLab has already conducted our open enrollment period. You should be prompted to complete this in Lumity after you finish electing your benefits for the current year. For more information on benefits in 2021, please review the [handbook](https://about.gitlab.com/handbook/total-rewards/benefits/general-and-entity-benefits/inc-benefits-us/#cigna-medical-plans-2021) and the [digital benefits guide](https://guides.lumity.com/f8wiCGbj/GitLab).
1. [ ] New team member: Review [GitLab's 401(k)](https://about.gitlab.com/handbook/total-rewards/benefits/general-and-entity-benefits/inc-benefits-us/#401k-plan) Plan. You will receive an email from Betterment within your first week, usually between Day 4-6. If you do not, please reach out to People Ops. 
   1. New team member: For more information on the plan, please review the [Summary Plan Description](https://drive.google.com/file/d/1k1xWqZ-2HjOWLv_oFCcyBkTjGnuv1l_B/view?usp=sharing) and [Fee Disclosure](https://drive.google.com/file/d/1gWxCI4XI01gofUu9yqKd4QyXeM27Xv57/view?usp=sharing).

</details>

<details>
<summary>People Experience, People Ops and Payroll</summary>

1. [ ] People Experience: Once the I9 is complete and audited, and the legal name in BambooHR has been checked, add a comment in the onboarding issue tagging the People Ops Analyst. State that the I9 process is completed and that the employment is authorized. Comment on Day 1 at the earliest.
1. [ ] People Experience: If new team member is an hourly employee, create a timesheet, share with Finance and the new team member. Also, send an email to Finance to let them know that an hourly employee has started.

</details>

<details>
<summary>Total Rewards</summary>

**To Do after I-9 is complete:**
1. [ ] Total Rewards Analyst: Once the BambooHR profile has been audited, check the "I-9 Processed" box located on the Personal tab in BambooHR and add the relevant [Benefit Group](https://docs.google.com/spreadsheets/d/1QU2rsFrrKSRQIrzWu2eqylK0HrNvt9FUhc_S5VQAVJ4/edit?usp=sharing) on the Benefits tab.
    * Comment in the issue with "Ready to add to ADP" to alert US Payroll to add team member to ADP.
    * Once new team member has been added to ADP, sync with benefits vendors and send invitations.

</details>

<details>
<summary>Payroll</summary>

1. [ ] Payroll (`@atiseo` `@csotomango`): Add new team member to ADP and send invitation to team member. 
1. [ ] Payroll (`@atiseo` `@csotomango`): Communicate with PeopleOps Analyst that team member has been successfully added.

</details>



