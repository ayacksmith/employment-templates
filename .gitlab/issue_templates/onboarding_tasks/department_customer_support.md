#### For Support Only

<details>
<summary>New Team Member</summary>

1. [ ] New team member: Familiarize yourself with the [engineering handbook](https://about.gitlab.com/handbook/engineering) and relevant pages linked from there.
1. [ ] New team member: Once you get ZenDesk access, complete the following.
    1. [ ] Add a profile picture to your Zendesk account
    1. [ ] Let your manager know if you were not able to create an account in Zendesk for some reason.
    1. [ ] Under your profile in Zendesk, it should read 'Support Staff'. If it reads Light Agent, inform your manager.
1. [ ] New team member: [Request access to ChatOps.](https://docs.gitlab.com/ee/development/chatops_on_gitlabcom.html#chatops-on-gitlabcom)
1. [ ] New team member: Team Calls, To keep up with the changes and happenings within the Support team, we have team calls every week. Every member of the support team is encouraged to join so they can also state their opinions during the call.
   1. [ ] Read up on the [calls the Support Team uses to sync up](https://about.gitlab.com/handbook/support/#weekly-meetings) and make sure you have the ones that pertain to you on your calendar.
   1. [ ] Identify agendas for those meetings, and read through a few past meetings in the document.
   1. [ ] Verify that you have a 1:1 scheduled with your manager and you have access to the agenda for that meeting.
1. [ ] New team member: Introduce yourself to your team!
   1. [ ] Write an entry with your name, location, the date you started, a quick blurb about your experience, personal interests and what drew you to GitLab support in the [Support Week in Review doc](https://docs.google.com/document/d/1eyMzbzImSKNFMpmu33C6imvC1iWEWHREJqaD6mkVDNg/edit) before Friday.
   1. [ ] Now, also format and send the introduction post you just created to the `#support_team-chat` Slack channel. Welcome to
      [multi-modal communication](https://about.gitlab.com/handbook/communication/#multimodal-communication), key to effective communication at GitLab!
1. [ ] New team member: Add the Support Time Off calendar as an [additional calendar in PTO by Roots](https://about.gitlab.com/handbook/support/support-time-off.html#one-time-setup-actions) to ensure any time off you schedule automatically gets added to the team calendar as well as your own.
1. [ ] New team member: Read about [Support's onboarding learning pathway](https://about.gitlab.com/handbook/support/training/#support-onboarding-pathway) to understand the different modules you'll be assigned based on your role.

</details>


<details>
<summary>Manager</summary>

1. [ ] Manager: At the beginning of the new team member's second week, open the [New Support Team Member Start Here](https://gitlab.com/gitlab-com/support/support-training/issues/new?issuable_template=New-Support-Team-Member-Start-Here) issue and provide the link in a comment below this onboarding checklist.
1. [ ] Manager: If the new team member will be a ZenDesk Admin, make sure to include that in the access request. 
1. [ ] Manager: If the new team member is going to be working US Federal tickets (U.S. Citizens only), open a [Individual or Bulk access request](https://gitlab.com/gitlab-com/team-member-epics/access-requests/-/issues/new?issuable_template=Individual_Bulk_Access_Request) to have them added as an agent in the Zendesk US Federal Instance and assign it to `@jcolyer`.
1. [ ] Manager: Add new team member to calendar event for:
   1. [ ] regional call
1. [ ] Manager: The new member will be added to the following calendars with inclusion in `GSuite supportgroup` as part of their standard entitlments.  If needed sooner than that issue being provisioned, you can [manually add new team member](https://support.google.com/calendar/answer/37082?hl=en#:~:text=Click%20More-,Settings%20and%20sharing.,Click%20Send.) to calendars.
   1. [ ] GitLab Support
   1. [ ] Support Time Off
1. [ ] Manager: Add new team member as an `Owner` to the GitLab.com testing groups:
   1. [ ] [GitLab.com Gold](https://gitlab.com/gitlab-gold)
   1. [ ] [GitLab.com Bronze](https://gitlab.com/gitlab-bronze)
   1. [ ] [GitLab.com Silver](https://gitlab.com/gitlab-silver)
1. [ ] Manager: Help your new team member to introduce themself
   1. [ ] Talk to them about doing an introduction at the next regional team meeting, and add it to the agenda.
</details>
