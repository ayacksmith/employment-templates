### For employees with GK only 

<details>
<summary>People Experience</summary>

#### Before Start Date

1. [ ] People Experience: Send the forms below for the new team member to fill out, with the hire date as the effective date, and upload to `Payroll` folder in BambooHR. This will enable our payroll provider to help us with the Health Insurance registration:  
  * [ ] [Employee Registration Form_BDO](https://docs.google.com/spreadsheets/d/146YDwvn8CZiiI1a1pBy7-kfERCQc-0fN/edit#gid=1937510953)
  * [ ] [Application for (change in) exemption for dependents of employment income earner](https://drive.google.com/drive/folders/1zQ0aYTeor59dszMfYH4j7D4a3OB8Z9_Q)
1. [ ] People Experience: Notify the payroll team that this has been done by emailing `nonuspayroll@gitlab.com`.
</details>
