#### For Product Management Only

<details>
<summary>New Team Member</summary>

1. [ ] New team member: Familiarize yourself with the [product stages](https://about.gitlab.com/stages-devops-lifecycle/).
1. [ ] New team member: For the [section, stage and group]((https://about.gitlab.com/handbook/product/categories/)) you are assigned, join the `#s_$STAGENAME` and `#g_GROUPNAME` Slack channels.
1. [ ] New team member: Update PM [mapping of your product categories](https://about.gitlab.com/handbook/product/categories/).
    - This is done by making updates in [`stages.yml`](https://gitlab.com/gitlab-com/www-gitlab-com/blob/master/data/stages.yml) and [`_categories.erb`](https://gitlab.com/gitlab-com/www-gitlab-com/blob/master/source/includes/product/_categories.erb)
1. [ ] New team member: Schedule introductory calls with your Core Team as listed in the [Product Categories page](https://about.gitlab.com/handbook/product/categories/).
   1. [ ] Scheduled call with _____ EM
   1. [ ] Scheduled call with _____ Product Designer
   1. [ ] Scheduled call with _____ PMM
   1. [ ] Scheduled call with _____ UX Researcher
1. [ ] New team member: Create an [Iteration Training](https://gitlab.com/gitlab-com/Product/-/issues/new?issuable_template=iteration-training) issue, assign it yourself, and complete the training. 

**User Research and Design**

1. [ ] New team member: Familiarize yourself with our customer [Roles and Personas](https://about.gitlab.com/handbook/marketing/product-marketing/roles-personas/)
1. [ ] New team member: Familiarize yourself with the capabilities and process of our [UX Research Team](https://about.gitlab.com/handbook/engineering/ux/ux-research/) including their [UX Insights issue repository](https://gitlab.com/gitlab-org/uxr_insights/issues?scope=all&utf8=%E2%9C%93&state=closed).
1. [ ] New team member: Familiarize yourself with [GitLab's Design System](http://design.gitlab.com).

**Access**

1. [ ] New team member: If you need access to Staging, if not already granted, please follow the instructions on the [Staging section](https://about.gitlab.com/handbook/engineering/infrastructure/environments/#staging) in the engineering handbook.
1. [ ] New team member: Request a light agent ZenDesk account to view customer tickets: [support handbook](https://about.gitlab.com/handbook/support/internal-support/#light-agent-zendesk-accounts-available-for-all-gitlab-staff)
1. [ ] New team member: Request access for Chorus.ai using [Okta](https://gitlab.okta.com/app/UserHome)

**Working as a PM**

1. [ ] New team member: Order and expense the book *Inspired:  How to Create Tech Products Customers Love* by Marty Cagan and read within your first two months at GitLab
1. [ ] New team member: Familiarize yourself with the [product handbook](https://about.gitlab.com/handbook/product) and relevant pages linked from there. Pay particular attention to:
   1. [ ] [Planning Horizon](https://about.gitlab.com/handbook/product/product-management/process/#planning-horizon)
   1. [ ] [Prioritization](https://about.gitlab.com/handbook/product/product-management/process/#prioritization)
   1. [ ] [Stakeholder Management](https://about.gitlab.com/handbook/product/product-management/process/#stakeholder-management)
   1. [ ] [GitLab as a Product](https://about.gitlab.com/handbook/product/#gitlab-as-a-product)
   1. [ ] [Product Development Flow](https://about.gitlab.com/handbook/product-development-flow/)
1. [ ] New team member: Review the [important dates PMs should keep in mind](https://about.gitlab.com/handbook/product/product-management/process/#important-dates-pms-should-keep-in-mind) and the [product development timeline](https://about.gitlab.com/handbook/engineering/workflow/#product-development-timeline).
1. [ ] New team member: Review the [product categories](https://about.gitlab.com/handbook/product/categories/) for the group you are assigned, along with the interfaces internal and external to GitLab.
1. [ ] New team member: Watch the [issue grooming training](https://www.youtube.com/watch?v=es-SuhU_6Rc) video 
1. [ ] New team member: Briefly review the process for [release posts](https://about.gitlab.com/handbook/marketing/blog/release-posts/)
1. [ ] New team member: Familiarize yourself with [Triage issues](https://gitlab.com/gitlab-org/gitlab/issues?scope=all&utf8=%E2%9C%93&state=opened&search=%22triage+report%22), which are created for groups automatically. Update [team-triage-package.yml](https://gitlab.com/gitlab-org/quality/triage-ops/blob/master/policies/stages/package/team-triage-package.yml) to assign these issues to yourself for your group.
1. [ ] New team member: Note that you will submit the first 3 issues you work on at Gitlab for an issue review. After that, it is an optional process per the handbook: [Review track](https://about.gitlab.com/handbook/product-development-flow/#review-track-optional).
1. [ ] New team member: Schedule a "shadowing" session with your buddy. Set aside a hour or two to observe him/her doing their work and ask questions along the way

 **PM Product and Company Onboarding**

1. [ ] New team member: Familiarize yourself with the company [Objective and Key Results](https://about.gitlab.com/company/okrs/), [how the Product team tracks them](https://about.gitlab.com/handbook/product/#objectives-and-key-results-okrs), and understand the implications for your stage.
1. [ ] New team member: Go through the [Product marketing demos](https://about.gitlab.com/handbook/marketing/product-marketing/demo/) to familiarize yourself with how we talk about our software to prospective customers.
1. [ ] New team member: Familiarize yourself with the Gitlab Unfiltered Youtube channel [Gitlab UN](https://www.youtube.com/channel/UCMtZ0sc1HHNtGGWZFDRTh5A)
1. [ ] New team member: Read the [stewardship](https://about.gitlab.com/company/stewardship/) page to learn how we think about our Stewardship of GitLab as an open source project.

**Iterate**

1. [ ] New team member: Make an update to the [Product Management section of the onboarding issue template](https://gitlab.com/gitlab-com/people-group/employment-templates/edit/master/.gitlab/issue_templates/onboarding_tasks/department_product_management.md).
1. [ ] New team member: Confirm you received access and can login to Chorus.ai. This should have been part of your [access request issue](https://gitlab.com/gitlab-com/team-member-epics/access-requests/-/blob/master/.gitlab/issue_templates/role_baseline_access_request_tasks/department_product_management/role_product_manager.md).

</details>

<details>
<summary>Manager</summary>

1. [ ] Manager: Add the new team member to our [asynchronous standup meetings in Slack with GeekBot](https://geekbot.com/dashboard/standup/34764/edit/advanced#members).
1. [ ] Manager: Create a [Product Onboarding ticket](https://gitlab.com/gitlab-com/Product/issues/new?issue) for the team member for ongoing onboard tasks. Customize it as appropriate given the team members start date relative to our release cycle and their specific team.

</details>

#### Introduction to Data

<details>
<summary>New Team Member</summary>

1. [ ] New team member: Consider joining the #data channel on Slack, as it's the best place to reach out to the data team.
1. [ ] New team member: Consider joining the #data-lounge channel on Slack, as it's where data-related resources get shared.
1. [ ] New team member: The data team project can be found at gitlab.com/gitlab-data/analytics. If you have data requests, be sure to create issues there.
1. [ ] New team member: Review the [Data for Product Managers](https://about.gitlab.com/handbook/business-ops/data-team/data-for-product-managers/) page in the handbook.
1. [ ] New team member: There is a lot more information on data at GitLab in the [Data Team Handbook](https://about.gitlab.com/handbook/business-ops/data-team/#data-team-handbook)
- [ ] New team member: Watch ["The State of Product Data"](https://www.youtube.com/watch?v=eNLkj3Ho2bk&feature=youtu.be) from Eli Kastelein at the Growth Fastboot. (You'll need to be logged into GitLab Unfiltered.)

</details>

##### Sisense (formerly Periscope)

<details>
<summary>New Team Member</summary>

Sisense (formerly Periscope) is GitLab's data analysis and business intelligence tool. You may see Sisense referred to as Periscope as their [name change is in progress](https://www.sisense.com/press-release/sisense-acquisition-of-periscope-yields-versatile-bi-platform/). Confirm you can access. This should have been part of your [access request issue](https://gitlab.com/gitlab-com/team-member-epics/access-requests/-/blob/master/.gitlab/issue_templates/role_baseline_access_request_tasks/department_product_management/role_product_manager.md).
1. [ ] New team member: Obtain access to [Periscope](https://about.gitlab.com/handbook/business-ops/data-team/periscope/), our data analysis tool.
   1. [ ] Review the [getting started with Periscope Data guide](https://doc.periscopedata.com/article/getting-started)
   1. [ ] Familiarize yourself with relevant [dashboards](https://app.periscopedata.com/app/gitlab/topic/Product/ab707846c91f4d30b1c1ca0399803d67).

</details>

##### Pings (Product)

<details>
<summary>New Team Member</summary>

This data comes from the usage ping that comes with a GitLab installation.
1. [ ] New team member: Read about the [usage ping](https://docs.gitlab.com/ee/user/admin_area/settings/usage_statistics.html).
1. [ ] New team member: To understand how this is implemented at GitLab read [Feature Implementation](https://about.gitlab.com/handbook/product/feature-instrumentation/#instrumentation-for-gitlabcom).
1. [ ] New team member: Read the product vision for [telemetry](https://about.gitlab.com/direction/telemetry/).
1. [ ] New team member: There is not great documentation on the usage ping, but you can get a sense from looking at the `usage.rb` file for [GitLab](https://gitlab.com/gitlab-org/gitlab/blob/master/lib/gitlab/usage_data.rb).
1. [ ] New team member: It might be helpful to look at issues related to the [usage pings (telemetry)](https://gitlab.com/gitlab-org/telemetry/issues) and [here](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=✓&state=all&search=~telemetry).
1. [ ] New team member: Watch the [pings brain dump session](https://drive.google.com/file/d/1S8lNyMdC3oXfCdWhY69Lx-tUVdL9SPFe/view).

</details>

##### Snowplow (Product)

<details>
<summary>New Team Member</summary>

[Snowplow](https://snowplowanalytics.com) is an open source web analytics collector.
1. [ ] New team member: To understand how this is implemented at GitLab read [Feature Implementation](https://about.gitlab.com/handbook/product/feature-instrumentation/#instrumentation-for-gitlabcom).
1. [ ] New team member: Also read how we pull data from [S3 into Snowflake](https://about.gitlab.com/handbook/business-ops/data-team/#snowplow-infrastructure)
1. [ ] New team member: Familiarize yourself with the [Snowplow Open Source documentation](https://github.com/snowplow/snowplow).
1. [ ] New team member: We use the [Snowplow dbt package](https://hub.getdbt.com/fishtown-analytics/snowplow/latest/) on our models. Their documentation does show up in our dbt docs.

</details>

##### Metrics and Methods

<details>
<summary>New Team Member</summary>

1. [ ] New team member: Read through [SaaS Metrics 2.0](http://www.forentrepreneurs.com/saas-metrics-2/) to get a good understanding of general SaaS metrics.
1. [ ] New team member: Check out [10 Reads for Data Scientists Getting Started with Business Models](https://www.conordewey.com/blog/10-reads-for-data-scientists-getting-started-with-business-models/) and read through the collection of articles to deepen your understanding of SaaS metrics.
1. [ ] New team member: Familiarize yourself with the GitLab Metrics Sheet (search in Google Drive, it should come up) which contains most of the key metrics we use at GitLab and the [definitions of these metrics](https://about.gitlab.com/handbook/finance/operating-metrics/).

</details>
