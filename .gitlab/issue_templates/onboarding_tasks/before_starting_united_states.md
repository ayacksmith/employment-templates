### Before Starting at GitLab: Team Members in the US 

<details>
<summary>People Experience</summary>

1. [ ] People Experience: Send the employee email instructions in LawLogix - 7 days prior to the start date. 
1. [ ] People Experience: Enter new team member's designated Section 2 Agent into LawLogix and send invite.
1. [ ] People Experience: Run the completed I-9 through e-verify within the LawLogix Guardian platform. 
   1. [ ] People Experience: After the case has been submitted to e-verify, check to see whether employment has been authorized or whether any further action is needed. Steps for photo match can be found in the [Handbook](https://about.gitlab.com/handbook/people-group/general-onboarding/onboarding-processes/#e-verify-photo-matching).
1. Once employment has been authorized, download and save the I-9 E-Verify confirmation form to the `Verification` folder in BambooHR, along with the supporting identity documents. 


</details>
