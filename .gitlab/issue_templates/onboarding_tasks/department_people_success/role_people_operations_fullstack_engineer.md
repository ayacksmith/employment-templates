## People Operations Fullstack Engineer
- [ ] Make yourself familiar with the [People Group Engineering handbook](https://about.gitlab.com/handbook/people-group/engineering/)
- [ ] Join the following channels on Slack: #peopleops-eng, #peopleops-alerts, #assessment-tool, #people-exp-ops
- [ ] Check out the [People Group Engineering projects](https://gitlab.com/gitlab-com/people-group/peopleops-eng) 
- [ ] Clone the [Employment Automation project](https://gitlab.com/gitlab-com/people-group/peopleops-eng/employment-automation) and follow the steps in the README to be able to run the specs locally. 
- [ ] Clone the [Nominator bot project](https://gitlab.com/gitlab-com/people-group/peopleops-eng/nominatorbot) and check if you can run the specs locally. 
- [ ] Clone the [Assessment project](https://gitlab.com/gitlab-com/people-group/peopleops-eng/assessment-tool) and check if you can run the specs locally. Once you have the right access to BambooHR, you should also be able to run the project locally. 
- [ ] Create a merge request to improve any of the project's README's that you've just cloned and worked on.
