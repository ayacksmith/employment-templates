### Welcome!

This **Career Mobility Issue** was compiled to offer you guidance and support as you migrate from your internship into your new role as a permanent GitLab team member. 

### Introduction
The Career Mobility Issue is split out into four areas of responsibility - namely those of the **Team Member** (__TEAM_MEMBER_HANDLE__); those of the **Previous Reporting Manager** (__PREVIOUS_MANAGER_HANDLE__); those of the **New Reporting Manager** (__MANAGER_HANDLE__) and finally those of the **People Experience Associate Team** (__PEOPLE_EXPERIENCE_HANDLE__).

If any of the tasks assigned to you seem confusing and you aren't sure how to proceed please be sure to let the People Experience Team know via the #peopleops Slack channel.  Alternatively if you are unable to work on a task because you are waiting on another stakeholder to close out one of theirs, feel free to ping them right here in this issue.  

In some instances your **New Reporting Manager** may opt to assign you a **Career Mobility Buddy** as an extra point of support, if this is the case please don't hesitate to route any queries you may have in their direction too.

### What to Expect
Though you recently will have completed the Intern Onboarding Issue we have identified a set of tasks that will make your migration to a permanent role within GitLab easy and efficient.  You are encouraged to complete each task over the course of two weeks paying careful attention to those which are compliance related (more information on that below).

| Area of Focus |
| --- |
| Profile Updates (Internal and External) |
| Security and System Refresher |
| BambooHR Review |
| Handbook Acknowledgement |
| Role Based Tasks |

### Cross-Boarding Compliance
It is important to note that certain tasks within your Career Mobility Issue relate to compliance and ultimately keeping GitLab secure.  These tasks are marked with a **red circle** and completion of them is subject to audit by both the People Experience Team and various other stakeholders such as **Security** and **Payroll** so please be sure to acknowledge them once complete by checking the relevant box.

### Continuous Improvement
In the interests of paying it forward and contributing to our core values of Collaboration and Efficiency - we encourage you to suggest changes and updates to this issue and the handbook all of which can be done via Merge Request (MR).

---

### Pre Migration Tasks

<details>
<summary>People Experience</summary>

1. [ ] Once notified of the migration by the People Operations Analyst in the Transfer Tracker, create a **confidential** issue called 'Career Mobility: (Team Member First Name and Surname), per (Date i.e. YYYY-MM-DD) as (Role Title).
1. [ ] Ensure that the issue has been assigned to all key stakeholders i.e. the team member and managers mentioned in the opening paragraph.
1. [ ] Check if the due date was correctly added i.e. **two weeks** from the effective date of migration i.e. the date on which the team member will start their new role by the Employment bot.
1. [ ] Previous Manager is `__PREVIOUS_MANAGER_HANDLE__`, new Manager is `__MANAGER_HANDLE__`, and People Experience is tackled by `__PEOPLE_EXPERIENCE_HANDLE__`. Check if the issue is assigned to the People Experience team member, the migrating team member, both the Previous and New Reporting Managers and the relevant People Business Partner.
1. [ ] Check that the New Manager has linked the relevant access requests in this issue for compliance.
1. [ ] Find the new team member's profile in [BambooHR](https://gitlab.bamboohr.com/home) and input [relevant data](https://about.gitlab.com/handbook/general-onboarding/onboarding-processes/) into the GitLab team member's profile.
1. [ ] :red_circle: If the new team member listed any exceptions to the IP agreement in their contract, choose `Yes` in the Exception to the IP agreement field in the Jobs tab. Email the VP of Engineering and VP of Product with listed exceptions for approval. Add a pdf of the email approval thread in the Contracts & Changes folder. Set the uploaded document to "shared" so that the team member can also view it.
1. [ ] :red_circle: If applicable, add a time off accrual plan for team members located in GitLab LTD (UK), GitLab GmbH (Germany), GitLab B.V. (Belgium), GitLab Inc (China), and GitLab B.V.(Netherlands). Click on "Time Off", hover your cursor over the "Employee Accruals" box, and you should see "Accrual Options" appear at the bottom. Click on it and select the appropriate policy from the dropdown. Click Save.
1. [ ] Enable "self-service" in BambooHR by clicking the orange gear at the top right, hovering over "BambooHR Access Level" and selecting "Employee self-service."
1. [ ] Depending on the team members location, please ping the relevant payroll contact persons in this issue to notify them that the team member is active again i.e. US and Canada: Ann Tiseo and Cristine Marquardt and Non-US: Harley Devlin and Nicole Precilla
1. [ ] Update the Status in BambooHR under the Personal Tab to Active. This should be done the business day prior to the team member coming back. 
1. [ ] :red_circle: Ping the ITOps team (`@gitlab-com/business-ops/team-member-enablement`) in this issue to reactivate all the accounts for the new team member.
1. [ ] Schedule a GSuite Email Reset to arrive at 7 am local time
1. [ ] Schedule a Welcome back email and include this Career Mobility Issue Link 


## Day 1 
1. [ ] Ping Total Rewards when ready for audit 

## After 1 Week 
1. [ ] Add a comment to this issue, checking in on the new team member at the end of week one. Check on their progress within the issue and be sure to reach out if you feel they have stalled or may have hit a block.

</details>

<details>
<summary>Total Rewards</summary>

1. [ ] In BambooHR, update access levels for Managers and Contractors, if applicable, as follows: 
   1. [ ] Employees who are Managers of people: "Managers".
   1. [ ] Contractors (independent or corp-to-corp): "Contractor Self-Service".
   1. [ ] Contractors who are Managers of people: "Multiple Access Levels": "Contractor Self-Service" and "Managers".
1. [ ] In BambooHR, audit the entry and mark complete in the "Payroll Change Report".
1. [ ] Add new team member to the comp calculator.

</details>

<details>
<summary>Previous Reporting Manager</summary>

1. [ ] Review the team member's current access and submit an [Access Change Request](https://gitlab.com/gitlab-com/team-member-epics/access-requests/issues/new?issuable_template=Access_Change_Request) issue. Please work with the New Reporting Manager to determine if a team member's access will still be needed in the new role or if access will need to be updated to a higher/lower access role. Please cc the new manager.
1. [ ] Coordinate with IT Ops to change any shared passwords, with the intent to be gated behind Okta when possible, in particular;
   1. [ ] Review if team member has sysadmin access passwords for GitLab.com Infrastructure (ssh, chef user/key, others). Identify if any can be moved to Okta.
   1. [ ] Review what 1Password vaults team member had access to, and identify any shared passwords to be changed and moved to Okta. If team member has access to any shared passwords that will no longer be relevant to their new role, please ping  `@gitlab-com/business-ops/team-member-enablement` and coordinate shared account password rotation.
1. [ ] Remove any scheduled 1:1 meetings with the team member.

</details>

<details>
<summary>New Reporting Manager</summary>

1. [ ] Review the team member's current access and submit an [Access Request](https://gitlab.com/gitlab-com/team-member-epics/access-requests/issues/new?issuable_template=Single_Person_Access_Request) or choose from a role based template in the [new issue dropdown](https://gitlab.com/gitlab-com/team-member-epics/access-requests/issues).
1. [ ] Be sure to link any Access Requests created to this Career Mobility Issue for ease of reference and in support of internal security compliance mechanisms.
1. [ ] Select an Career Mobility Buddy on the new employee's team.  Please try to select someone in a similar time zone, and someone who has been at GitLab for at least 3 months.
1. [ ] Send an email to the chosen buddy `MANAGER__PLACE__BUDDY_HANDLE__HERE` letting them know that they have been chosen, and include a link to the career mobility issue.
1. [ ] Schedule a video call using Zoom with the new team member at the start of their first day to welcome them to the team and set expectations.
1. [ ] Organize smooth career mobility with clear starting tasks / pathway for new team member.
1. [ ] Calendars and Agendas:
   1. [ ] Invite team member to recurring team meetings.
   1. [ ] Schedule weekly [1:1 meetings](https://about.gitlab.com/handbook/leadership/1-1/) with new team member. Use the first 15 minutes to get to know your new team member on a personal level.
1. [ ] Set new GitLab team members' project-level permissions as-needed.
1. [ ] Add the new team member to the required groups in 1Password if you have access to the Admin Console in 1Password. If you do not have access please ping @gitlab-com/business-ops/team-member-enablement with which vaults the new team member should be added to. Note: if it is necessary to add an individual to a vault (instead of to a group), make sure that the permissions are set to _not allow_ exporting items.
1. [ ] Introduce your new team member to your team at your next team call. Ask them to tell the team about where they were before GitLab, why they wanted to join the team, where they are located, and what they like to do for fun outside of work.
1. [ ] Review [GitLab's Probation Period Process](https://about.gitlab.com/handbook/people-group/contracts-and-international-expansion/#probation-period) and check if your new team member falls within a country that has a probation period. This is important as the People Experience team will be following up with you to review the probation period, within the period on the linked page.

</details>

<details>
<summary>Career Mobility Buddy</summary>

1. [ ] Review your schedule, if you will be taking extended PTO (more than a few days) or going out on leave, please alert the team members manager and ask for a new Career Mobility Buddy to be assigned. 
1. [ ] Review the [Onboarding Buddies handbook page](https://about.gitlab.com/handbook/general-onboarding/onboarding-buddies/) and become familiar with your role. These guidelines were written for onboarding but many can still apply to assist in career mobility. 
1. [ ] Schedule a zoom call during their first week to introduce yourself. Make sure they know that if they have questions, they can come to you. Guide them to the right parts of the handbook, and/or encourage them to ask their questions of the wider group in Slack, etc.
1. [ ] Schedule at least one follow-up chat after one week. 
1. [ ] Consider iterating on the [career mobility issue template](https://gitlab.com/gitlab-com/people-group/employment-templates/-/blob/master/.gitlab/issue_templates/career_mobility.md) or suggest improvements or give feedback on the #peopleops slack channel.
---
</details>

### Team Member Tasks

<details>
<summary>Team Member</summary>

#### Profile Updates
1. [ ] Update team page entry. Instructions here in [the handbook](https://about.gitlab.com/handbook/git-page-update/#11-add-yourself-to-the-team-page).
1. [ ] Update your [Slack profile](https://gitlab.slack.com/account/profile) to include your new role.
1. [ ] Update your GitLab profile to include your new role.
1. [ ] Update your Gmail signature to include your new role. [Here is an example you can use](https://about.gitlab.com/handbook/tools-and-tips/#sts=Email%20signature).
1. [ ] Update your [Zoom profile](https://zoom.us/profile) to include your new role.
1. [ ] Update your LinkedIn profile to reflect your new position with GitLab. Consider sharing a link to your LinkedIn profile in any Slack channel with your immediate team members and including a message inviting them to connect with you there. 

### General Refresher

#### Security
1. [ ] :red_circle: Read our [Security Practices](https://about.gitlab.com/handbook/security) page, especially our Password Policy Guidelines.
#### 1Password 
1. [ ] :red_circle: Confirm that your 1Password vault is active. GitLab provides all team members with an account to 1Password. Some teams use 1Password for shared notes and some shared accounts.
   1. [ ] If uninstalled, reinstall the 1Password app on your computer, and link it to your team account, as described on the [security practices page](https://about.gitlab.com/handbook/security/#adding-the-gitlab-team-to-a-1password-app). Please let People Experience or your manager know if you need any assistance.
   1. [ ] Read 1Password's [getting started guides](https://support.1password.com/explore/extension/) and the [Mac app](https://support.1password.com/getting-started-mac/) to understand its functionality.
   1. [ ] Ensure that your 1Password master key is unique and random. Start learning it!
#### Okta
1. [ ] :red_circle: Check that your Okta Account is active. Reminder: GitLab uses Okta as its primary portal for all SaaS Applications. Okta will populate your Dashboard with many of the Applications you will need, but some of these will require activation. Read and Review the [Okta Handbook](https://about.gitlab.com/handbook/business-ops/okta/) page for more information. If inactive, please ping ITOPs (`@gitlab-com/business-ops/team-member-enablement`) in this issue for further assistance. 
   1. [ ] Reset your GitLab password via Okta if needed. Make sure that you start from the [Okta dashboard](https://gitlab.okta.com/app/UserHome) to log into your GitLab account. Do this by clicking the GitLab.com tile directly from your Okta dashboard.
   1. [ ] Reset your Google password via Okta if needed and re set up 2FA for your Google account.
   1. [ ] Reset any additional existing passwords in line with the security training & guidelines, if needed.
   1. [ ] Confirm in the comment box that you are using 1Password and Okta in accordance with our Security Practices.
#### 2FA (Two-Factor Authentication)
1. [ ] :red_circle: GitLab requires you to enable 2FA (2-Step Verification, also known as two-factor authentication). Please make sure that time is set automatically on your device (ie. on Android: "Settings" > "Date & Time" > "Set automatically"). If you have problems with 2FA just let IT Ops know in the #it_help Slack channel! We can disable it for you to then set it up again. If the problem persists, then we can direct you to one of our super admins. Reach out proactively if you're struggling - it's better than getting locked out and needing to wait for IT Ops to help you get back online.
1. [ ] Please confirm in this issue that 2FA has been set up on all your accounts. 
#### BambooHR
1. [ ] :red_circle: Check that you have access to BambooHR and that all personal details listed are correct.
1. [ ] On the More tab, click Bank Information and check that your bank details are correct (Not applicable to Safeguard, CXC & Global Upside team members).
1. [ ] Reminder, for contractors, it may be useful to save an invoice template for future use. An invoice template can be found in Google Docs by searching `Invoice Template` or [here](https://docs.google.com/spreadsheets/d/1CxJMQ06GK_DCqihVaZ0PXxNhumQYzgG--nw_ibPV0XA/edit#gid=0).
1. [ ] Don't forget to comply with the contract you signed, and make sure you understand [Intellectual Property](https://about.gitlab.com/handbook/people-group/code-of-conduct/#intellectual-property-and-protecting-ip).
1. [ ] Check out the [People Operations handbook page](https://about.gitlab.com/handbook/people-group/) and learn more about the People team and everything under our purview.
#### Values
1. [ ] GitLab values are a living document. In many instances, they have been documented, refined, and revised based on lessons learned (and scars earned) in the course of doing business. Familiarize yourself with our [Values page](https://about.gitlab.com/handbook/values/).
#### Calendar:
1. [ ] Verify you still have access to view the GitLab Team Meetings calendar. Go to your calendar, left sidebar, go to Other calendars, press the + sign, Subscribe to calendar, and enter in the search field `gitlab.com_6ekbk8ffqnkus3qpj9o26rqejg@group.calendar.google.com` and then press enter on your keyboard. Reach out to a People Experience Associate if you have any questions. NOTE: Please do NOT remove any meetings from this calendar or any other shared calendars, as it removes the event from everyone's calendar. 
#### Zoom
1. [ ] Check that you still have access to your Zoom account via Okta. 
1. [ ] Review [GitLab's internal acceptable use policy](https://about.gitlab.com/handbook/people-group/acceptable-use-policy/)
#### Social
1. [ ] Consider having a few coffee chats with fellow team members, we know its been a little while since your internship ended, might be nice to catchup. 
1. [ ] Reminder about the [#donut-be-strangers](https://gitlab.slack.com/messages/C613ZTNEL/) channel in Slack, which will automatically set you up on one random [coffee break call](https://about.gitlab.com/company/culture/all-remote/informal-communication/#coffee-chats) every other week. 
1. [ ] Check that you will have access to the [GitLab Unfiltered YouTube channel](https://www.youtube.com/channel/UCMtZ0sc1HHNtGGWZFDRTh5A). 
#### Git
1. [ ] Download and [get started with Git](https://docs.gitlab.com/ce/gitlab-basics/start-using-git.html). 
1. [ ] Reminder, if you need any general help with Git, ask in the [`#questions`](https://gitlab.slack.com/archives/questions) channel in Slack. For questions with Merge Requests, ask in the [`#mr-buddies`](https://gitlab.slack.com/archives/mr-buddies) channel. With specific questions using Git in the terminal, subscribe to the [`#git-help`](https://gitlab.slack.com/archives/git-help) channel on Slack and feel free to ask any questions you might have.
#### Tools
1. [ ] We use TripActions for company travel; please [create your account through Okta](https://gitlab.okta.com/app/UserHome). Head to the [travel page](https://about.gitlab.com/handbook/travel/#setting-up-your-tripactions-account) for further instructions on how to setup your account.
1. [ ] Review the Gitlab [Organizational Chart](https://about.gitlab.com/company/team/org-chart/) to learn how our company is structured and who reports to whom. You can also review our [Organizational Structure](https://about.gitlab.com/company/team/structure/) that gives additional information on how we structure the organization and job families. 
#### Your Workspace
1. [ ] Take some time to consider the setup of your workspace. Check out these helpful Handbook pages on how to [Create an Ergonomic Workspace](https://about.gitlab.com/company/culture/all-remote/tips/#create-an-ergonomic-workspace) & what [Supplies](https://about.gitlab.com/handbook/spending-company-money/#office-equipment-and-supplies) you can purchase to enable this. Having an ergonomic and comfortable workspace will enable the utmost productivity. Set yourself up for success as you start your second week at GitLab.
#### Handbook
1. [ ] Since we are a global organization understand your benefits might be different than other team members at the company based on which [contract](https://about.gitlab.com/handbook/contracts/) you signed. It is important to understand **your** [benefits](https://about.gitlab.com/handbook/benefits/). If you have questions please reach out to People Experience.
1. [ ] Put some time aside to uncover any new information in the team [Handbook](https://about.gitlab.com/handbook/) or a simply refresher. A core value of GitLab is documentation. Therefore, everything that we do, we have documented in the handbook. 
1. [ ] Reminder that there is a [no ask time off policy](https://about.gitlab.com/handbook/paid-time-off): GitLab truly does value a work-life balance, and encourages team members to have a flexible schedule and take vacations. If you feel uncomfortable about taking time off, or are not sure how much time to take off throughout the year, feel free to speak with your manager or People Experience. We will be happy to reinforce this policy! Please note the additional steps that might need to be taken if you are scheduled for [on call](https://about.gitlab.com/handbook/on-call).
1. [ ] Understanding that Diversity & Inclusion is one of GitLab's top priorities please take a look at GitLab's Diversity & Inclusion [page](https://about.gitlab.com/company/culture/inclusion/)
1. [ ] Read about GitLab [stock options](https://about.gitlab.com/handbook/stock-options/). If you received stock options as part of your contract, your options will be approved by the Board of Directors at their quarterly board meetings. After your grant has been approved by the Board you will receive a grant notice by email.
1. [ ] Review our [Team Member social media guidelines](https://about.gitlab.com/handbook/marketing/social-media-guidelines/) for both using your own social media channels and how to be an ambassador for the company.

</details>

### Role-specific tasks

<!-- include: division_tasks -->

---

<!-- include: department_tasks -->

---

<!-- include: role_tasks -->


/label ~"career-mobility"

